#!/bin/bash
#Dependencias: modprobed-db, sed, mkinitcpio. (Solo funciona en arch linux)
#Los archivos se crearán en la carpeta "kernelbuild" del home, si no existe dicha carpeta será creada 
num=$(($(nproc) + 1))
echo Contraseña:
read -s pas
if ! [ -d ~/kernelbuild ]; 
then
	mkdir ~/kernelbuild
fi
cd ~/kernelbuild
if [ -f ./linux-4.$1.tar.xz ]
then
else
	wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.$1.tar.xz
fi
if [ -f ./linux-4.$1.tar.xz ];
then
	tar -xvJf linux-4.$1.tar.xz
	cd ./linux-4.$1
	make clean && make mrproper
	make LSMOD=$HOME/.config/modprobed.db localmodconfig
	#Si se quiere editar algún parametro más descomenta la siguiente línea
	#make nconfig
	make CFLAGS="-march=native -O2 -pipe" CXXFLAGS="-march=native -O2 -pipe" -j$num
	echo $pas | sudo -S make modules_install
	#Estos módulos se gaurdarán en la carpera "/usr/lib/modules/", con un rm -r se pueden eliminar
	if [ -f arch/x86_64/boot/bzImage ];
	then
		sudo cp -v arch/x86_64/boot/bzImage /boot/vmlinuz-linuxcustom
		if ! [ -f /etc/mkinitcpio.d/linuxcustom.preset ];
		then
			sudo cp /etc/mkinitcpio.d/linux.preset /etc/mkinitcpio.d/linuxcustom.preset
			sudo sed -i 's/linux/linuxcustom/g' "/etc/mkinitcpio.d/linuxcustom.preset"
		fi
		sudo mkinitcpio -p linuxcustom
		#Si no se usa grub comenta o elimina la siguiente línea
		sudo grub-mkconfig -o /boot/grub/grub.cfg
		echo kernel compilado correctamente
	else
		echo fallo al compilar
	fi
else
	echo version inaccesible
fi